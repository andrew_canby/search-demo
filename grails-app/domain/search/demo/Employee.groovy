package search.demo

class Employee {

    static searchable = {
        office component: true
    }

    String firstName
    String surname

    Date dateOfBirth

    int salary

    Office office

    static constraints = {
    }
}
