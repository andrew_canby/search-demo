package search.demo

class Office {

    static searchable = {
        root false
    }

    String suburb

    String address

    static constraints = {
    }
}
