package search.demo

class SearchController {
    static responseFormats = ['json']

    def list() {
        def results = Employee.search("${params.query}",
                [from: params.from, size: params.size, sort: params.sort])
        render view: 'list', model: [results: results]
    }
}
