package search.demo

class UrlMappings {

    static mappings = {

        "/search/list"(controller: "Search", action: "list")

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
