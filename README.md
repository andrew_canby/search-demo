# README #

Quick demo for ElasticSearch within a Groovy/Grails app

### What is this repository for? ###

* Elastic Search
* Grails 3.2.0

### How do I get set up? ###

* ./gradlew bootRun
* Make calls to the `/search/list` endpoint to get some results. The `query` param takes lucene syntax, but you'll need to URL encode `&&` and `||` to make more complex searches.

Eg:

* http://localhost:8080/search/list?query=john - find all employees with any mention of 'john' (uses the all index)
* http://localhost:8080/search/list?query=firstName:john - find all employees with a first name of 'john'
* http://localhost:8080/search/list?from=5&size=40&sort=salary&query=firstName:john%20%7C%7C%20surname:Fletcher - Find 40 employees starting at result 5 with first name 'john' or last name 'fletcher', sorted by salary